<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'w2;K*AHYnph01z+{OH$I~?bF<!ldEVTpcyIZ}K)?/$Bwb!c(Ki.k9G/rE*3Wm&c1' );
define( 'SECURE_AUTH_KEY',  '}{oiP^QxWWKl$cA`(#03DjS,&Az|P9ZJbOSNA#ZVh<Vgh>5xvT2;YUCCm9.k{(wz' );
define( 'LOGGED_IN_KEY',    '5o){%;4>~4;$wb$n Shk[zPF7G2ePr0+qp}HY-VyAK?1HQ?2=~#?[3L!I|~:1~7F' );
define( 'NONCE_KEY',        '5B8<6#qK;Cpn0{U/2`a}tB`hq!1sZI]#hf:/HNaRv}HiffRST* v<!y_L!WjG:tj' );
define( 'AUTH_SALT',        '`ojvu=c:V~sYV#]<UzN -c-wzFj3?I@kLRp/I)<]MMg5tU04SM-./Cohj nKR|]d' );
define( 'SECURE_AUTH_SALT', '<DHK[.tF{FgA70@my+3L;D.ZM?DTDZ:a;`e<x!A?Mo2sE*AZ1Z6~2nmv,X53%Y+!' );
define( 'LOGGED_IN_SALT',   'Q[PsA#}vA:m**[F&m:`A9<I6hLbkE5zYB(4m8;LvRkEhJ&L:jD>8(ctp7.$CwaoW' );
define( 'NONCE_SALT',       'MY7Q)`V(]wyn*}T}k8|.o)*<P8gK{#G+0(IoPkwq4EF]A`~9J;{.6KeP^dY~t;FJ' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
